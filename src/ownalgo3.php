<?php
	// senti_algo_fb('motorola');
	// print_r(senti_algo_flip('lg_flip'));
	// print_r(sentiment("good phone"));
	$m1 = new MongoClient();
	$db = $m1->selectDB("major_db");
	$collection = $db->selectCollection('results');
	$moto_res=array('samsung' => array(
		'facebook' => array_count_values(senti_algo_fb('samsung_fb')) ,
		'cnet' => array_count_values(senti_algo_cnet('samsung_cnet')) ,
		'flipkart' => array_count_values(senti_algo_flip('samsung_flip'))
		)
	);
	// $moto_res=array('title' => 'samsung' , 'facebook' => array(
	// 															'1' => '1317',
	// 															'-1' => '609',
	// 															'0' => '3058'),
	// 										'cnet'=> array_count_values(senti_algo_cnet('samsung_cnet')),
	// 										'flipkart' => array('1' => '458',
	// 															'-1' => '181',
	// 															'0' => '105'));
	// $collection->insert($moto_res);
	// $m1->close();

	function senti_algo_flip($src){
		$m = new MongoClient();
		$db = $m->selectDB("major_db");
		$collection = $db->selectCollection($src);
		$cursor = $collection->find();
		$array = iterator_to_array($cursor);
		$flip_crawl=reset($array);
		$sentiment=array();
		echo '<pre>';
		// print_r($flip_crawl);
		$j=0;
		foreach ($flip_crawl as $key => $value) {
			if($key=='_id' || $key=='title')
				continue;
			$comments=$value['comments'];
			foreach ($comments as $key1 => $value1) {
				$review=$value1['review'];
				$sentiment[$j++]=sentiment($review);

			}
			echo '<pre>';
			// $sentiment[$j++]=sentiment($review);
		}
		$m->close();
		return ($sentiment);	
	}

	function senti_algo_cnet($src){
		$m = new MongoClient();
		$db = $m->selectDB("major_db");
		$collection = $db->selectCollection($src);
		$cursor = $collection->find();
		$array = iterator_to_array($cursor);
		$cnet_crawl=reset($array);
		$sentiment=array();
		echo '<pre>';
		// print_r($cnet_crawl);
		$j=0;
		foreach ($cnet_crawl as $key => $value) {
			if($key=='_id' || $key=='title')
				continue;
			$review=$value['review'];
			echo '<pre>';
			$sentiment[$j++]=sentiment($review);
		}
		$m->close();
		return ($sentiment);	
	}

	function senti_algo_fb($src){
		$m = new MongoClient();
		$db = $m->selectDB("major_db");
		$collection = $db->selectCollection($src);
		$cursor = $collection->find();
		$array = iterator_to_array($cursor);
		$fb_crawl=reset($array);
		$sentiment=array();

		foreach ($fb_crawl as $key => $value) {
			if($key=='_id' || $key=='title')
				continue;
			$status=$value['statuses'];
			echo '<pre>';
			foreach ($status as $key1 => $value1) {
				// print_r($value1['text']);
				$k=$value1['id'];
				$sentiment[$k]=sentiment($value1['text']);
				foreach ($value1['comments'] as $key2 => $value2) {
					$j=$value2['id'];
					$sentiment[$j]=sentiment($value2['text']);
					// print_r($value2['text']);
				}
			}
		}
		$m->close();
		return ($sentiment);	
	}
	
	function sentiment($sentence){
		$result=0;
		$positive_words = array();
		$negative_words = array();
		$neutral_words = array();
		$l_sentence = strtolower($sentence);
		$wordslist = explode(" ",$l_sentence);
		$words = $wordslist;
		$prior_positive = $prior_negative = 0.3333333333;
		$prior_neutral = 0.3333333334;
		$positive_count = $negative_count = $neutral_count = 0;
		
		$file_content = file_get_contents("data/positive.php");
		$pvocabs = unserialize($file_content);
		
		$file_content = file_get_contents("data/negative.php");
		$nvocabs = unserialize($file_content);
		
		$file_content = file_get_contents("data/neutral.php");
		$svocabs = unserialize($file_content);
		
		foreach($words as $word){
			foreach($pvocabs as $pvocab)
			{
				if(!strcmp($word,$pvocab))
				{
					array_push($positive_words,"$word");
					$positive_count++;
					$found = 1;
					$prior_positive = $prior_positive * 2;
					$sum=$prior_positive+$prior_negative+$prior_neutral;
					$prior_positive=$prior_positive/$sum;
					$prior_negative=$prior_negative/$sum;
					$prior_neutral=$prior_neutral/$sum;
					break;
				}
			}
			
			foreach($nvocabs as $nvocab)
			{
				if(!strcmp($word,$nvocab))
				{
					array_push($negative_words,"$word");
					$negative_count++;
					$found = 1;
					$prior_negative = $prior_negative * 2;
					$sum=$prior_positive+$prior_negative+$prior_neutral;
					$prior_positive=$prior_positive/$sum;
					$prior_negative=$prior_negative/$sum;
					$prior_neutral=$prior_neutral/$sum;
					break;
				}
			}
			
			
			foreach($svocabs as $svocab)
			{
				if(!strcmp($word,$svocab))
				{
					array_push($neutral_words,"$word");
					$neutral_count++;
					$found = 1;
					$prior_neutral = $prior_neutral * 2;
					$sum=$prior_positive+$prior_negative+$prior_neutral;
					$prior_positive=$prior_positive/$sum;
					$prior_negative=$prior_negative/$sum;
					$prior_neutral=$prior_neutral/$sum;
					break;
				}
			}
		}
			
		$total=$positive_count+$negative_count+$neutral_count;
		$total=$total/2;
		
		if ($prior_positive > $prior_negative && $prior_positive > $prior_neutral) 
		{
			$result=1;
			// file_put_contents("C:/xampp1/htdocs/Major/src/resultaddp.txt","ADDP:POSITIVE ");
		}
		else if ($prior_negative > $prior_positive && $prior_negative > $prior_neutral) 
		{
			$result=-1;
			// file_put_contents("C:/xampp1/htdocs/Major/src/resultaddp.txt","ADDP:NEGATIVE ");
		}
		else if ($prior_neutral > $prior_positive && $prior_negative < $prior_neutral) 
		{
			if ($neutral_count < $total)
			{
				$result=0;
				// file_put_contents("C:/xampp1/htdocs/Major/src/resultaddp.txt","ADDP:NEUTRAL ");
			}
			else if($prior_positive > $prior_negative)
			{
				$result= 1;
				// file_put_contents("C:/xampp1/htdocs/Major/src/resultaddp.txt","ADDP:POSITIVE ");
			}
			else if($prior_positive < $prior_negative)
			{
				$result= -1;
				// file_put_contents("C:/xampp1/htdocs/Major/src/resultaddp.txt","ADDP:NEGATIVE ");
			}
			else
			{
				$result= 0;
				// file_put_contents("C:/xampp1/htdocs/Major/src/resultaddp.txt","ADDP:NEUTRAL ");
			}
		}
		return $result;
	}
?>