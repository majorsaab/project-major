<?php
	use	Everyman\Neo4j\Relationship;
	require('../lib/neo4jphp/vendor/autoload.php'); 

	$google=array(0=>'google');
	$yahoo=array(0=>'yahoo');
	$bing=array(0=>'bing');
    $client = new Everyman\Neo4j\Client('localhost', 7474);
   	$label2= $client->makeLabel('engine');
	$label = $client->makeLabel('page');
	$bing_arr=process('http://wwww.bing.com',1);
	$yahoo_arr=process('http://wwww.yahoo.com',2);
	$google_arr=process('http://wwww.google.com',3);

	$result=array_unique(array_intersect($yahoo_arr, $bing_arr, $google_arr));

	$pre=$label2->getNodes("url", 'http://wwww.google.com');
	$pre_id=$pre[0]->getId();
	$google=$client->getNode($pre_id);

	$pre=$label2->getNodes("url", 'http://wwww.bing.com');
	$pre_id=$pre[0]->getId();
	$bing=$client->getNode($pre_id);

	$pre=$label2->getNodes("url", 'http://wwww.yahoo.com');
	$pre_id=$pre[0]->getId();
	$yahoo=$client->getNode($pre_id);

	echo'<pre>';
	// print_r($result);
	$res2=path_count($result);
	$res=array_count_values(find_path($result));
	$res1=array_merge($res2,path_count($res,0));
	arsort($res1);
	$o=0;
	// print_r($res1);
	foreach ($res1 as $key => $value) {
		if($o++ ==5){
			break;
		}
		# code...
		$arr=explode('->', $key);
		echo strip_tags("<a href=".$arr[0].">");
		// echo "<a href=$arr[0]";
		print_r($arr[0]);
		echo strip_tags("</a>");
		echo '<br>';


	}
		
	function path_count($array,$flag=1){
		global $client,$label2;
		$pre=$label2->getNodes("url", 'http://wwww.google.com');
		$pre_id=$pre[0]->getId();
		$google=$client->getNode($pre_id);

		$pre=$label2->getNodes("url", 'http://wwww.bing.com');
		$pre_id=$pre[0]->getId();
		$bing=$client->getNode($pre_id);

		$pre=$label2->getNodes("url", 'http://wwww.yahoo.com');
		$pre_id=$pre[0]->getId();
		$yahoo=$client->getNode($pre_id);
	  	$arr=array();
	  	foreach ($array as $key => $value){
			if($flag==1){
				$arr1=explode("->",$value);
				$start=$client->getNode($arr1[1]);
			}
			else{
				$arr1[1]=$key;
				$start=$client->getNode($arr1[1]);
				$value=$start->getProperty('url').'->'.$key;
			}

	    	$path1 = $google->findPathsTo($start ,'point_to', Relationship::DirectionOut)
	    				  ->setMaxDepth(5)
	  	  				  ->getPaths();	
	  
	  	    $path2 = $bing->findPathsTo($start, 'point_to', Relationship::DirectionOut)
	  	    			->setMaxDepth(5)
	  					  ->getPaths();
	  
	  	    $path3 = $yahoo->findPathsTo($start , 'point_to', Relationship::DirectionOut)
	  	    			->setMaxDepth(5)
	  					  ->getPaths();

	  		$count=count($path1)+count($path2)+count($path3);

	  		$arr[$value]=$count;
	  	}
	  	return $arr;

	}

	function find_path($array){
		global $client,$label2;
		$pre=$label2->getNodes("url", 'http://wwww.google.com');
		$pre_id=$pre[0]->getId();
		$google=$client->getNode($pre_id);

		$pre=$label2->getNodes("url", 'http://wwww.bing.com');
		$pre_id=$pre[0]->getId();
		$bing=$client->getNode($pre_id);

		$pre=$label2->getNodes("url", 'http://wwww.yahoo.com');
		$pre_id=$pre[0]->getId();
		$yahoo=$client->getNode($pre_id);
		// echo 'in find path';
		$k=0;
		$arr=array();
		foreach ($array as $key=>$value) {
			$arr1=explode("->",$value);
			$endNode=$client->getNode($arr1[1]);
			$node_out = $endNode->getRelationships(array('point_to'),Relationship::DirectionIn);
			
			foreach ($node_out as $relation) {
			    $start=$relation->getStartNode();
			    $g=$b=$y=0;

			    $path1 = $google->findPathsTo($start, 'point_to', Relationship::DirectionOut)
    	  	    			->setMaxDepth(5)
	    					  ->getPaths();
		    	if(!empty($path1)){
		    		$g++;
		    	}

		    	$path2 = $yahoo->findPathsTo($start, 'point_to', Relationship::DirectionOut)
		  	    			->setMaxDepth(5)
	    					  ->getPaths();
		    	if(!empty($path2)){
		    		$y++;
		    	}
		    	$path3 = $bing->findPathsTo($start, 'point_to', Relationship::DirectionOut)
		    		  	    ->setMaxDepth(5)
	    					  ->getPaths();
		    	if(!empty($path3)){
		    		$b++;
		    	}

		    	if($g>0 && $b>0 && $y>0){
		    		$arr[$k++]=$start->getId();

		    	}

			}	
		}
		return $arr;
	}

	function process($url,$flag){
		global $label2,$client;
		$array=array();

		$pre=$label2->getNodes("url", $url);
		$pre_id=$pre[0]->getId();
		$start=$client->getNode($pre_id);
		get_last($start, $array);	
		return $array; 
	}
	
	function get_last($node, &$arr){
		$node_out = $node->getRelationships(array('point_to'),Relationship::DirectionOut);
 		if(empty($node_out)){
 			// echo '<br>';
 			array_push($arr,$node->getProperty('url').'->'.$node->getId());
 			return;
 		}
	    foreach ($node_out as $relation) {
		    $end=$relation->getEndNode();
		    get_last($end, $arr);
		}	
	}
?>