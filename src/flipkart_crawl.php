<?php
	require('../lib/simple_html_dom.php');
	$file = fopen("../additional data/samsung_flipkart.csv","r");
	$opts = array('http' => array('header' => "User-Agent:MyAgent/1.0\r\n"));
	$context = stream_context_create($opts)	;
	$m = new MongoClient();
    $db = $m->major_db;
    $collection = $db->samsung_flip;
	$pages=array();
	$cnt=0;
	while(!empty($file) && $cnt<85) 
	{
		// this is for samsung. since those url couldnt be found so the following condition:
		if($cnt==78 || $cnt==26 || $cnt==122 || $cnt==32 || $cnt==126 || $cnt ==62){
			$cnt++;
			continue;			
		}
		$content=fgetcsv($file);
		$url=$content['1'];
		$id=$content['0'];
		if($url!='null'){
			$url_used=stristr($url,'&affid=',true);
			$html = file_get_html($url_used, FALSE, $context);
			$comments=array();
			foreach($html->find('div[class=rightCol]') as $element) {
			    echo '<pre>';
			    $title=$element->children[0];
			    $review=$element->children[1];
			    if($review->find('span[class=review-text-full]')){
			    	$review=$review->children[1];
			    }
			    $left=$element->prev_sibling();
			    $date=$left->children[2]->innertext();
			    $comment=array(
			    	'title' => trim($title->innertext()),
			    	'review' => trim($review->innertext()) , 
			    	'date' => trim($date)
			    	);
				array_push($comments,$comment);
			}
			$product_pg=array('id' => $id ,'comments' => $comments);
			array_push($pages,$product_pg);
			echo $cnt++.'<br>';			
		}
	}
	$document=array('title' => 'flipkart');
	$result=array_merge($document,$pages);
	print_r($result);
	$collection->insert($result);
	fclose($file);
	$m->close();
?>