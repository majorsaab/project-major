<?php // content="text/plain; charset=utf-8"
	require_once ('../lib/jpgraph/src/jpgraph.php');
	require_once ('../lib/jpgraph/src/jpgraph_line.php');

	$moto=process('motorola');
	$lg=process('lg');
	$sam=process('samsung');
	// echo '<pre>';
	// print_r($moto);
	// print_r($lg);
	// print_r($sam);
	// die(0);

	$datay1 = array(0, $moto['fb'] , $moto['cnet'] , $moto['flip']);
	$datay2 = array(0, $lg['fb'] , $lg['cnet'] , $lg['flip']);
	$datay3 = array(0, $sam['fb'] , $sam['cnet'] , $sam['flip']);

	// $datay1 = array(0, $moto['fb'] , $moto['cnet'] , $moto['lg']);
	// $datay2 = array(0, $lg['fb'] , $lg['cnet'] , $lg['flip']);
	// $datay3 = array(0, $sam['fb'] , $sam['cnet'] , $sam['flip']);

	// Setup the graph
	$graph = new Graph(450,400);
	$graph->SetScale("textlin");

	$theme_class=new UniversalTheme;

	$graph->SetTheme($theme_class);
	$graph->img->SetAntiAliasing(false);
	$graph->title->Set('Popularity Plot');
	$graph->SetBox(false);

	$graph->img->SetAntiAliasing();

	$graph->yaxis->HideZeroLabel(true);
	$graph->yaxis->HideLine(false);
	$graph->yaxis->HideTicks(false,false);

	$graph->xgrid->Show();
	$graph->xgrid->SetLineStyle("solid");
	$graph->xaxis->SetTickLabels(array(' ','Facebook','Cnet','Flipkart'));
	$graph->xgrid->SetColor('#E3E3E3');

	// Create the first line
	$p1 = new LinePlot($datay1);
	$graph->Add($p1);
	$p1->SetColor("#6495ED");
	$p1->SetLegend('Motorola');

	// Create the second line
	$p2 = new LinePlot($datay2);
	$graph->Add($p2);
	$p2->SetColor("#B22222");
	$p2->SetLegend('LG');

	// Create the third line
	$p3 = new LinePlot($datay3);
	$graph->Add($p3);
	$p3->SetColor("#FF1493");
	$p3->SetLegend('Samsung');


	$graph->legend->SetFrameWeight(1);

	// Output line
	$graph->Stroke();

	function process($make){
		$m1 = new MongoClient();
		$db = $m1->selectDB("major_db");
		$collection = $db->selectCollection('results');
		$cursor = $collection->find(array('title'=> $make));
		$array = iterator_to_array($cursor);
		$elem=reset($array);
		$m1->close();

		$fb=$elem['facebook']['-1'] + $elem['facebook']['0'] + $elem['facebook']['1'];
		$cnet=$elem['cnet']['-1'] + $elem['cnet']['0'] + $elem['cnet']['1'];
		$flip=$elem['flipkart']['-1'] + $elem['flipkart']['0'] + $elem['flipkart']['1'];	

		$data=array('fb'=> $fb , 'cnet' => $cnet , 'flip' => $flip);
		return $data;
	}

?>