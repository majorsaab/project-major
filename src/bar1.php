<?php // content="text/plain; charset=utf-8"
	require_once ('../lib/jpgraph/src/jpgraph.php');
	require_once ('../lib/jpgraph/src/jpgraph_bar.php');
	require_once ('../lib/jpgraph/src/jpgraph_mgraph.php');

	$graph1=process('motorola');
	$graph2=process('lg');
	$graph3=process('samsung');
	$graph3->Stroke();
	die(0);
	$mgraph = new MGraph();
	$xpos1=100;$ypos1=5;
	$xpos2=100;$ypos2=600;
	$xpos3=100;$ypos3=1200;
	$mgraph->Add($graph1,$xpos1,$ypos1);
	$mgraph->Add($graph2,$xpos2,$ypos2);
	$mgraph->Add($graph3,$xpos3,$ypos3);
	$mgraph->Stroke();

	function process($make){
		$m1 = new MongoClient();
		$db = $m1->selectDB("major_db");
		$collection = $db->selectCollection('results');
		$cursor = $collection->find(array('title'=> $make));
		$array = iterator_to_array($cursor);
		$elem=reset($array);
		$m1->close();

		$moto_fb=$elem['facebook'];
		$moto_cnet=$elem['cnet'];
		$moto_flip=$elem['flipkart'];	
		if($make=='samsung')
			$moto_fb['0']=1350;
		$data1ym=array($moto_fb['-1'], $moto_flip['-1'], $moto_cnet['-1']);
		$data2ym=array($moto_fb['0'], $moto_flip['0'], $moto_flip['-1']);
		$data3ym=array($moto_fb['1'], $moto_flip['1'], $moto_cnet['1']);
		return (plot($make,$data1ym,$data2ym,$data3ym));
	}

	function plot($make,$data1y,$data2y,$data3y){
		$graph = new Graph(350,500,'auto');
		$graph->SetScale("textlin");

		$theme_class=new UniversalTheme;
		$graph->SetTheme($theme_class);

		$graph->yaxis->SetTickPositions(array(0,100,200,300,400,500,600,700,800,900,1000,1100,1200,1300), array(50,150,250,350,450,550,650,750,850,950,1050,1150,1250,1350));
		$graph->SetBox(false);

		$graph->ygrid->SetFill(false);
		$graph->xaxis->SetTickLabels(array('Facebook','Cnet','Flipkart'));
		$graph->yaxis->HideLine(false);
		$graph->yaxis->HideTicks(false,false);

		$b1plot = new BarPlot($data1y);
		$b2plot = new BarPlot($data2y);
		$b3plot = new BarPlot($data3y);

		$gbplot = new GroupBarPlot(array($b1plot,$b2plot,$b3plot));
		$graph->Add($gbplot);

		$b1plot->SetColor("white");
		$b1plot->SetFillColor("#cc1111");
		$b1plot->setLegend("Negative");

		$b2plot->SetColor("white");
		$b2plot->SetFillColor("#11cccc");
		$b2plot->setLegend("Neutral");

		$b3plot->SetColor("white");
		$b3plot->SetFillColor("#008000");
		$b3plot->setLegend("Positive");

		$graph->legend->SetFrameWeight(2);
		$graph->legend->SetColumns(3);
		$graph->legend->SetColor('#4E4E4E','#00A78A');
		
		$graph->title->Set("$make across different ");

		return $graph;
	}
?>