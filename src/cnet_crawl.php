<?php
	require('../lib/simple_html_dom.php');
	$opts = array('http' => array('header' => "User-Agent:MyAgent/1.0\r\n"));
	$context = stream_context_create($opts)	;
	$html = file_get_html("http://www.cnet.com/topics/phones/products/motorola/", FALSE, $context);
	echo '<pre>';
	$cnt=1;
	$pg_cnt=0;
	$m = new MongoClient();
    $db = $m->major_db;
    $collection = $db->moto_cnet;
	$pages=array();
	do{
		$next=$html->find('div[class=pageNav] a[class=next]');
		foreach($html->find('section[class=searchItem product]') as $element ){
			//for if conditions only for sony since few pages didnt had reviewDate on them. so to skip them these conditions:			
			// if($cnt==11 || $cnt==14){
			// 	$cnt++;
			// 	continue;

			// }
			// if($cnt==50){
			// 	break;
			// }
			$review_pg=file_get_html("http://www.cnet.com".$element->children[0]->href, FALSE , $context);
			$review=" ";
			$date=$review_pg->find('div[class=reviewDate] span time');
			
			$review_date=trim($date[0]->innertext());
			$title = $review_pg->find('span[class=itemreviewed]');
			$review_title=trim($title[0]->innertext());
			foreach($review_pg->find('div[class=description]') as $content){
				$para1=$content->children();
				foreach($para1 as $para){
					if($para->nodeName()=='p'  && trim($para->innertext) != null) 
					{
						if( $para->hasChildNodes() == FALSE){
							$review=$review.trim($para->innertext());							
						}
						else if( $para->hasChildNodes() == TRUE && $para->children[0]->nodeName() != 'figure'){
							$review=$review.trim($para->innertext());
						}
					}
				}
			}
			$review_array=array('date' => $review_date, 'review' => $review ,'review title' => $review_title);
			array_push($pages,$review_array);
			echo ' '.$cnt++;
		}
		$html= file_get_html("http://www.cnet.com".$next[0]->href, FALSE ,$context);
		$pg_cnt++;
		echo '<br>';
	}while($next[0]->innertext == 'Next' && $pg_cnt<5);
	$document=array('title' => 'cnet');
	$result=array_merge($document,$pages);
	print_r($result);
	$collection->insert($result);
	$m->close();
?>