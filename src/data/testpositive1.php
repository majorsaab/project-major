austin powers in goldmember has the right stuff for silly summer entertainment and has enough laughs to sustain interest to the end
schaeffer isn't in this film which may be why it works as well as it does
a fresh entertaining comedy that looks at relationships minus traditional gender roles
surprisingly the film is a hilarious adventure and i shamelessly enjoyed it
the ally honest and told with humor and poignancy which makes its message resonate
if you can read the subtitles the opera is sung in italian and you like masterpiece theatre type costumes you enjoy this movie
a pretty funny movie with most of the humor coming as before from the incongruous but chemically perfect teaming of crystal and de niro
gangster no is solid satisfying fare for adults
this chicago has hugely imaginative and successful casting to its great credit as well as one terrific score and attitude to spare
has enough gun battles and throwaway humor to cover up the yawning chasm where the plot should be
as it stands crocodile hunter has the hurried badly cobbled look of the godzilla which combined scenes of a japanese monster flick with canned shotsof raymond burr commenting on the monster path of destruction
insomnia loses points when it surrenders to a formulaic bang-bang shoot them up scene at the conclusion but the performances of pacino williams and swank keep the viewer wide awake all the way through
what might have been readily dismissed as the tiresome rant of an aging film maker still thumbing his nose at convention takes a surprising subtle turn at the midway point
at a time when commercialism has squeezed the life out of whatever idealism american moviemaking ever had godfrey reggio career shines like a lonely beacon
an inuit masterpiece that will give you goosebumps as its uncanny tale of love communal discord and justice unfolds
this is popcorn movie fun with equal doses of action cheese ham and cheek as well as a serious debt to the road warrior but it feels like unrealized potential
it a testament to de niro and director michael caton jones that by movie end we accept the characters and the film flaws and all
performances are potent and the women stories are ably intercut and involving
an enormously entertaining movie like nothing we have ever seen before and yet completely familiar
lan yu is a genuine love story full of traditional layers of awakening and ripening and separation and recovery
pulls off the rare trick of recreating not only the look of a certain era but also the feel
twohy a good yarn spinner and ultimately the story compels
tobey maguire is a poster boy for the geek generation
a sweetly affecting story about four sisters who are coping in one way or another with life endgame
passion melodrama sorrow laugther and tears cascade over the screen effortlessly
road to perdition does display greatness and it worth seeing but it also comes with the laziness and arrogance of a thing that already knows it won
a marvelous performance by allison lohman as an identity seeking foster child
arliss howard ambitious moving and adventurous directorial debut big bad love meets so many of the challenges it poses for itself that one can forgive the film its flaws
what a dumb fun curiously adolescent movie this is
many insightful moments
a vivid sometimes surreal glimpse into the mysteries of human behavior
a tour de force of modern cinema
peralta captures in luminous interviews and amazingly evocative film from three decades ago the essence of the dogtown experience
the lively appeal of the last kiss lies in the ease with which it integrates thoughtfulness and pasta fagioli comedy
the performances are an absolute joy
a quasi documentary by french film maker karim dridi that celebrates the hardy spirit of cuban music
grant carries the day with impeccable comic timing raffish charm and piercing intellect
both exuberantly romantic and serenely melancholy what time is it there may prove to be tsai masterpiece
mazel tov to a film about a family joyous life acting on the yiddish stage
standing in the shadows of motown is the best kind of documentary one that makes a depleted yesterday feel very much like a brand new tomorrow
provides a porthole into that noble trembling incoherence that defines us all
schaeffer isn't in this film which may be why it works as well as it does